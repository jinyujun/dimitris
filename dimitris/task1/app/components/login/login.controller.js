(function() {

'use strict';

angular.module('login')
	.controller('LoginController', LoginController);

LoginController.$inject = ['$scope'];
/* ngInject */
function LoginController($scope) {
	var login = this;

	login.wants = ['buy', 'sell', 'rent', 'loan'];

	login.whoes = ['artist', 'advisor', 'dealer', 'collector', 'gallery', 'auctioneer', 'institution'];

	login.who = '';

	login.setWho = setWho;

	login.email = '';

	login.pass = '';

	login.emailError = '';

	login.passError = '';

	login.validate = validate;

	function setWho(who) {
		login.who = who;
	}

	function validate() {
		login.emailError = '';
		login.passError = '';

		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    	if(re.test(login.email) === false) {
    		login.emailError = 'invalid email!';
    	}
    	if(login.email === '') {
    		login.emailError = 'input email please.';
    	}
    	if(login.pass === '') {
    		login.passError = 'input password please.';
    	}
	}
}

})()