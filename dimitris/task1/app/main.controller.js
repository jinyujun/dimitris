(function() {

'use strict';

angular.module('app')
	.controller('MainController', MainController);

MainController.$inject = ['$scope'];
/* ngInject */
function MainController($scope) {
	var main = this;

	main.status = '';

	main.openWindow = openWindow;

	function openWindow(status) {
		main.status = status;
	}
}

})()